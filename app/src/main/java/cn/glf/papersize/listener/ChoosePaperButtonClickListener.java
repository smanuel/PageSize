package cn.glf.papersize.listener;

import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.math.BigDecimal;

import cn.glf.papersize.MainActivity;
import cn.glf.papersize.R;
import cn.glf.papersize.bean.CutSolution;
import cn.glf.papersize.bean.Paper;
import cn.glf.papersize.util.PageSizeUtil;

/**
 * Created by glf on 18-5-21.
 */

public class ChoosePaperButtonClickListener implements View.OnClickListener{
    private static final String LOGTAG = ChoosePaperButtonClickListener.class.getName();

    private MainActivity activity = null;

    public ChoosePaperButtonClickListener(MainActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onClick(View view) {
        ScrollView scrollView = activity.findViewById(R.id.scrollView);
        EditText widthText = activity.findViewById(R.id.textInputNeedPaperWidth);
        EditText heightText = activity.findViewById(R.id.textInputNeedPaperHeight);
        TextView resultText = activity.findViewById(R.id.textViewResult);
        TextView detailText = activity.findViewById(R.id.textViewDetail);
        ImageView img = activity.findViewById(R.id.imageView);
        //初始化
        resultText.setText("");
        detailText.setText("");
        img.setImageDrawable(null);
        hideKeyboard();
        scrollView.smoothScrollTo(0, 0);

        BigDecimal width = BigDecimal.ZERO;
        BigDecimal height = BigDecimal.ZERO;
        try{
            width = new BigDecimal(widthText.getText().toString());
            height = new BigDecimal(heightText.getText().toString());
        }catch (Exception e){
            resultText.setText("请正确输入所需纸品规格！");
        }

        //显示结果文字
        Paper needPaper = new Paper(height, width);
        CutSolution solution = PageSizeUtil.chooseBestCutSolution(needPaper);
        resultText.setText("最节省纸的选择为:\n\n" + solution.toString());

        //显示图片
        String imageFileName = solution.getCutPaper().getId() + solution.getCutPaper().getType() + "_" + solution.getCutMethod().getType();
        Class drawable = R.drawable.class;
        try {
            Field field = drawable.getField(imageFileName);
            img.setImageResource(field.getInt(imageFileName));
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        //显示详情
        showDetail(solution);
    }

    private void showDetail(CutSolution solution){
        TextView detailText = activity.findViewById(R.id.textViewDetail);
        StringBuilder stringBuilder = new StringBuilder("四种切割方法对比：\n\n");

        for(CutSolution detail : solution.getDetails()){
            stringBuilder.append(detail.toString()).append("\n\n");
        }

        detailText.setText(stringBuilder.toString());
    }

    private void hideKeyboard(){
        try {
            ((InputMethodManager) activity.getBaseContext().getSystemService(activity.getBaseContext().INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
