package cn.glf.papersize;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import cn.glf.papersize.listener.ChoosePaperButtonClickListener;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = (Button)findViewById(R.id.buttonChoosePaper);

        //绑定监听
        button.setOnClickListener(new ChoosePaperButtonClickListener(MainActivity.this));
    }
}
