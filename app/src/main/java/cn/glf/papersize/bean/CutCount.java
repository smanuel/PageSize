package cn.glf.papersize.bean;

import java.math.BigDecimal;

/**
 * 在一张纸张上的切割数量
 * Created by glf on 18-5-20.
 */

public class CutCount {

    BigDecimal widthCount = BigDecimal.ZERO;//在宽度方向数量

    BigDecimal heightCount = BigDecimal.ZERO;//在高度方向数量

    CutCount widthChildren = null;

    CutCount heightChildren = null;

    public CutCount() {
    }

    public CutCount(BigDecimal widthCount, BigDecimal heightCount) {
        this.widthCount = widthCount;
        this.heightCount = heightCount;
    }

    @Override
    public String toString() {
        return "CutCount{" +
                "widthCount=" + widthCount +
                ", heightCount=" + heightCount +
                ", totalCount=" + getTotalCount() +
                ", widthChildren=" + widthChildren +
                ", heightChildren=" + heightChildren +
                '}';
    }

    public BigDecimal getTotalCount(){
        return widthCount.multiply(heightCount);
    }

    public BigDecimal getWidthCount() {
        return widthCount;
    }

    public void setWidthCount(BigDecimal widthCount) {
        this.widthCount = widthCount;
    }

    public BigDecimal getHeightCount() {
        return heightCount;
    }

    public void setHeightCount(BigDecimal heightCount) {
        this.heightCount = heightCount;
    }

    public CutCount getWidthChildren() {
        return widthChildren;
    }

    public void setWidthChildren(CutCount widthChildren) {
        this.widthChildren = widthChildren;
    }

    public CutCount getHeightChildren() {
        return heightChildren;
    }

    public void setHeightChildren(CutCount heightChildren) {
        this.heightChildren = heightChildren;
    }
}
