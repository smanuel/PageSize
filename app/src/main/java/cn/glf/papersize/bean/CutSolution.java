package cn.glf.papersize.bean;

import cn.glf.papersize.util.PageSizeUtil;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 切割解决方案
 * Created by glf on 18-5-20.
 */

public class CutSolution {

    Paper needPaper = null;

    Paper cutPaper = null;

    CutMethod cutMethod = null;

    CutCount cutCount = null;

    List<CutSolution> details = new ArrayList();

    public CutSolution() {
    }

    public CutSolution(Paper needPaper) {
        this.needPaper = needPaper;
    }

    public CutSolution(Paper needPaper, Paper cutPaper, CutMethod cutMethod, CutCount cutCount) {
        this.needPaper = needPaper;
        this.cutPaper = cutPaper;
        this.cutMethod = cutMethod;
        this.cutCount = cutCount;
    }

    @Override
    public String toString() {
        DecimalFormat df=new DecimalFormat("0.00");
        return  cutPaper.getDescription() + "。\n" +
                "使用[" + cutMethod.getName() + "]方法切割。\n" +
                "每张可切割出 " + getAllTotalCount() + " 张所需纸品。\n" +
                "其中：主要部分可切割出 " + cutCount.getTotalCount()
                    + " 张，遗留部分可切割出 " + getLeftTotalCount() + "张。\n" +
                "每张最终剩余不可切割部分(即浪费部分)总面积大约为 " + getLeftArea().divide(needPaper.getArea(), 2, BigDecimal.ROUND_HALF_UP) + " 张所需纸品。\n" +
                "";
    }

    /**
     * 获取总共可以切多少张纸
     * @return
     */
    public BigDecimal getAllTotalCount(){
        return PageSizeUtil.getTotalCount(cutCount);
    }

    /**
     * 获取其中遗留部分可以切多少张纸
     * @return
     */
    public BigDecimal getLeftTotalCount(){
        return PageSizeUtil.getTotalCount(cutCount.getWidthChildren()).add(PageSizeUtil.getTotalCount(cutCount.getHeightChildren()));
    }

    /**
     * 获取剩余部分的面积
     * @return
     */
    public BigDecimal getLeftArea(){
        return cutPaper.getArea().subtract(needPaper.getArea().multiply(getAllTotalCount()));
    }

    public CutCount getCutCount() {
        return cutCount;
    }

    public void setCutCount(CutCount cutCount) {
        this.cutCount = cutCount;
    }

    public Paper getNeedPaper() {
        return needPaper;
    }

    public void setNeedPaper(Paper needPaper) {
        this.needPaper = needPaper;
    }

    public Paper getCutPaper() {
        return cutPaper;
    }

    public void setCutPaper(Paper cutPaper) {
        this.cutPaper = cutPaper;
    }

    public CutMethod getCutMethod() {
        return cutMethod;
    }

    public void setCutMethod(CutMethod cutMethod) {
        this.cutMethod = cutMethod;
    }

    public List<CutSolution> getDetails() {
        return details;
    }

    public void setDetails(List<CutSolution> details) {
        this.details = details;
    }
}
