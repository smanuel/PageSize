package cn.glf.papersize.bean;

/**
 * Created by glf on 18-5-20.
 */

public class CutMethod {
    int type = 0;
    String name = "";

    public CutMethod(int type, String name) {
        this.type = type;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CutMethod cutMethod = (CutMethod) o;

        if (type != cutMethod.type) return false;
        return name != null ? name.equals(cutMethod.name) : cutMethod.name == null;
    }

    @Override
    public int hashCode() {
        int result = type;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "CutMethod{" +
                "type=" + type +
                ", name='" + name + '\'' +
                '}';
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
