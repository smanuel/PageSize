package cn.glf.papersize.bean;

import java.math.BigDecimal;

/**
 * 纸张
 * Created by glf on 18-5-20.
 */

public class Paper {

    BigDecimal width = BigDecimal.ZERO;

    BigDecimal height = BigDecimal.ZERO;

    String name = "";

    String id = "";

    public Paper() {
    }

    public Paper(BigDecimal height, BigDecimal width) {
        this.width = width;
        this.height = height;
    }

    public Paper(BigDecimal height, BigDecimal width, String name, String id) {
        this.width = width;
        this.height = height;
        this.name = name;
        this.id = id;
    }

    /**
     * 获取面积
     * @return
     */
    public BigDecimal getArea(){
        return width.multiply(height);
    }

    /**
     * 获取描述
     * @return
     */
    public String getDescription(){
        return name + ": " + width.intValue() + " x " + height.intValue();
    }

    /**
     * 获取规格
     * @return
     */
    public String getType(){
        return width.intValue() + "x" + height.intValue();
    }

    @Override
    public String toString() {
        return "Paper{" +
                "width=" + width.toString() +
                ", height=" + height.toString() +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getWidth() {
        return width;
    }

    public void setWidth(BigDecimal width) {
        this.width = width;
    }

    public BigDecimal getHeight() {
        return height;
    }

    public void setHeight(BigDecimal height) {
        this.height = height;
    }
}
